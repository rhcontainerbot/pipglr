# pipglr/Containerfile
#
# Builds a Podman-in-Podman Gitlab-Runner image for
# executing Gitlab CI/CD jobs.  Requires configuration
# steps specific to Gitlab projects. For more info. see
# https://docs.gitlab.com/runner/executors/docker.html#use-podman-to-run-docker-commands
#

FROM quay.io/podman/stable:v4.3.1

# This is a list of packages to remove and/or exclude from the image.
# Primarily this is done for security reasons, should a runner process
# escape confinement.  Having fewer things to poke, lowers the attack
# surface-area.
#
# This list was formed manually by running these commands in the base image:
#   for package in $(rpm -qa); do \
#       if dnf erase $package; then echo "$package" >> exclude; fi; \
#   done; \
#   cat exclude
#
# After adding those packages to this file, the container build was run
# and package list adjusted, untill no dependency errors were raised.
ARG EXCLUDE_PACKAGES="\
    fedora-repos-modular \
    findutils \
    libxcrypt-compat \
    openldap-compat \
    podman-gvproxy \
    rootfiles \
    sudo \
    vim-minimal \
    yum"

# Base-image runs as user 'podman', temporarily switch to root
# for installation/setup.
USER root
# Helper for comparison in future RUN operations (DO NOT USE)
ARG _DNFCMD="dnf --setopt=tsflags=nodocs -y"
# Set this instead, if (for example) you want to volume-mount in /var/cache/dnf
ARG DNFCMD="${_DNFCMD}"
# Avoid installing any documentation to keep image small
# During install, excluding packages is meaningless if already installed
RUN set -x && \
    rm -f /etc/dnf/protected.d/sudo.conf && \
    rm -f /etc/dnf/protected.d/yum.conf && \
    $DNFCMD remove ${EXCLUDE_PACKAGES}

# Enable callers to customize the runner version as needed, otherwise
# assume this image will be version-tagged, so it's fine to grab the latest.
ARG RUNNER_VERSION="latest"
# When building a multi-arch manifest-list, this buid-arg is set automatically.
ARG TARGETARCH="amd64"
ENV RUNNER_RPM_URL=https://gitlab-runner-downloads.s3.amazonaws.com/${RUNNER_VERSION}/rpm/gitlab-runner_${TARGETARCH}.rpm
RUN for rpm in ${EXCLUDE_PACKAGES}; do x+="--exclude=$rpm "; done && \
    set -x && \
    $DNFCMD update && \
    $DNFCMD install $x $RUNNER_RPM_URL && \
    $DNFCMD upgrade && \
    $DNFCMD reinstall shadow-utils && \
    if [[ "${DNFCMD}" == "${_DNFCMD}" ]]; then \
      dnf clean all && \
      rm -rf /var/cache/dnf; \
    fi

# In case of a runner escape, prevent easy installation of packages.
RUN rm -f /etc/dnf/protected.d/* && \
    rpm -e dnf && \
    rm -f $(type -P rpm)

ADD /config.toml /home/podman/.gitlab-runner/config.toml
# The global "listen_address" option is used for metrics and
# debugging.  Disable it by default since use requires special/
# additional host configuration.
# Ref: https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-global-section
ARG RUNNER_LISTEN_ADDRESS="disabled"
ENV RUNNER_LISTEN_ADDRESS=$RUNNER_LISTEN_ADDRESS
RUN if [[ "$RUNNER_LISTEN_ADDRESS" == "disabled" ]]; then \
        sed -i -r \
            -e "s/.*@@RUNNER_LISTEN_ADDRESS@@.*//g" \
            /home/podman/.gitlab-runner/config.toml; \
    else \
        sed -i -r \
            -e "s/@@RUNNER_LISTEN_ADDRESS@@/$RUNNER_LISTEN_ADDRESS/g" \
            /home/podman/.gitlab-runner/config.toml; \
    fi

# A small wrapper is needed to launch a background podman system service
# process for the gitlab-runner to connect to.
ADD /gitlab-runner-wrapper /podman-in-podman-maintenance /usr/local/bin/
# Base image UTS NS configuration causes runner to break when launching
# nested rootless containers.
RUN sed -i -r \
        -e 's/^utsns.+host.*/utsns="private"/' \
        /etc/containers/containers.conf && \
    chmod +x /usr/local/bin/gitlab-runner-wrapper && \
    chmod +x /usr/local/bin/podman-in-podman-maintenance && \
    chown -R podman:podman /home/podman && \
    chmod u+s /usr/bin/new{uid,gid}map && \
    rm -f /home/podman/.bash* && \
    echo DOCKER_HOST="unix:///tmp/podman-run-1000/podman/podman.sock" > /etc/profile.d/podman.sh && \
    echo "podman:10000:10000" | tee /etc/subuid > /etc/subgid && \
    setcap -n 10000 cap_setuid+ep /usr/bin/newuidmap && \
    setcap -n 10000 cap_setuid+ep /usr/bin/newgidmap

# Runtime rootless-mode configuration
USER podman
# N/B: Volumes are cumulative with the base image
VOLUME ["/home/podman/.gitlab-runner/", "/cache"]
WORKDIR /home/podman
ENTRYPOINT ["/usr/local/bin/gitlab-runner-wrapper"]

# Ensure root storage directory exists with correct permissions
RUN mkdir -p .local/share/containers/storage

# Gitlab-runner configuration options.  Default to unprivileged (nested)
# runner.  Privileged is required to permit nested container image building.
ARG RUNNER_NAME="qontainers-pipglr"
# Running inner-podman privileged is necessary at the time of this commit.
ARG PRIVILEGED_RUNNER="true"
# Tags allow pinning jobs to specific runners, comma-separated list of
# tags to add to runner (no spaces!)
ARG RUNNER_TAGS="podman-in-podman"
# Permit running jobs without any tag at all
ARG RUNNER_UNTAGGED="true"
# Adjust based on usage and storage size to prevent ENOSPACE problems
ARG CLEAN_INTERVAL="24h"
ENV CLEAN_INTERVAL="$CLEAN_INTERVAL" \
    REGISTER_NON_INTERACTIVE="true" \
    RUNNER_TAG_LIST="$RUNNER_TAGS" \
    REGISTER_RUN_UNTAGGED="$RUNNER_UNTAGGED" \
    REGISTER_ACCESS_LEVEL="ref_protected" \
    REGISTER_MAXIMUM_TIMEOUT="3600" \
    CI_SERVER_URL="https://gitlab.com/" \
    RUNNER_NAME="${RUNNER_NAME}" \
    RUNNER_EXECUTOR="docker" \
    RUNNER_SHELL="bash" \
    REGISTER_MAINTENANCE_NOTE="Podman-in-Podman containerized runner" \
    DOCKER_HOST="unix:///tmp/podman-run-1000/podman/podman.sock" \
    DOCKER_DEVICES="/dev/fuse" \
    DOCKER_IMAGE="registry.fedoraproject.org/fedora-minimal:latest" \
    DOCKER_CACHE_DIR="/cache" \
    DOCKER_VOLUMES="/cache" \
    DOCKER_NETWORK_MODE="host" \
    DOCKER_PRIVILEGED="$PRIVILEGED_RUNNER"

# Not a real build-arg.  Simply here to save lots of typing.
ARG _pm="--systemd=true --device=/dev/fuse --security-opt label=disable --user podman --volume pipglr-podman-root:/home/podman/.local/share/containers --volume pipglr-config:/home/podman/.gitlab-runner -v pipglr-podman-cache:/cache --tmpfs /var/lib/containers,ro,size=1k -e PODMAN_RUNNER_DEBUG -e LOG_LEVEL"

# These labels simply make it easier to register and execute the runner.
# Define them last so they are absent should a image-build failure occur.
LABEL register="podman run -it --rm $_pm --secret REGISTRATION_TOKEN,type=env \$IMAGE register"
# Note: Privileged mode is required to permit building container images with inner-podman
LABEL run="podman run -d --privileged --name pipglr $_pm \$IMAGE run"

# In case it's helpful, include the documentation
ADD /README.md /home/podman/
